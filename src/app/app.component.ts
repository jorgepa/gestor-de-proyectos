import { Component } from '@angular/core';
import { Proyecto } from './clases/proyecto';
import { Equipos } from './clases/equipos';
import { Integrantes } from './clases/integrantes';
import { Actividades } from './clases/actividades';
import { TiemposEntregables } from './clases/tiempos-entregables';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'gestorProyectos';
  arrayProyectos : Proyecto [] = [];
  arrayEquipos : Equipos [] = [];
  arrayIntegrantes : Integrantes [] = [];
  arrayActividades : Actividades [] = [];
  arrayEntregables : TiemposEntregables [] = [];
  proyecto : Proyecto = new Proyecto();
  equipo : Equipos = new Equipos();
  integrante : Integrantes = new Integrantes();
  actividad : Actividades = new Actividades();
  entregable : TiemposEntregables = new TiemposEntregables();
  btnProy : boolean = false;
  btnEqu : boolean = false;
  btnInt : boolean = false;
  btnAct : boolean = false;
  btnEnt : boolean = false;

  crearProyecto(){
    this.proyecto.id = this.proyecto.aumentarId(this.arrayProyectos.length);
    this.arrayProyectos.push(this.proyecto);
    this.proyecto = new Proyecto();
  }

  mostrarBotones(nom:string, dato? : any){
    if(nom == "proyecto"){
      this.btnProy = true;
      this.proyecto = dato;
    }
    else if (nom == "equipo"){
      this.btnEqu = true;
      this.equipo = dato;
    }
    else if (nom == "integrante"){
      this.btnInt = true;
      this.integrante = dato;
    }
    else if (nom == "actividad"){
      this.btnAct = true;
      this.actividad = dato;
    }
    else if (nom == "entregable"){
      this.btnEnt = true;
      this.entregable = dato;
    }
  
  }  
  editarProyecto(){
    this.btnProy = false;
    this.proyecto = new Proyecto(); 
  }
  eliminarProyecto(){
    this.btnProy = false;
    this.arrayProyectos = this.arrayProyectos.filter(x => x!= this.proyecto);
    this.proyecto = new Proyecto();
  }
  crearEquipo(){
    this.equipo.id = this.proyecto.aumentarId(this.arrayEquipos.length);
    this.arrayEquipos.push(this.equipo);
    this.equipo = new Equipos();
  }
  editarEquipo(){
    this.btnEqu = false;
    this.equipo = new Equipos(); 
  }
  eliminarEquipo(){
    this.btnEqu = false;
    this.arrayEquipos = this.arrayEquipos.filter(x => x!= this.equipo);
    this.equipo = new Equipos();
  }

  crearIntegrante(){
    this.integrante.id = this.proyecto.aumentarId(this.arrayEquipos.length);
    this.arrayIntegrantes.push(this.integrante);
    this.integrante = new Integrantes();
  }
  editarIntegrante(){
    this.btnInt = false;
    this.integrante = new Integrantes(); 
  }
  eliminarIntegrante(){
    this.btnInt = false;
    this.arrayIntegrantes = this.arrayIntegrantes.filter(x => x!= this.integrante);
    this.integrante = new Integrantes();
  }

  crearActividad(){
    this.actividad.id = this.actividad.aumentarId(this.arrayActividades.length);
    this.arrayActividades.push(this.actividad);
    this.actividad = new Actividades();
  }
  editarActividad(){
    this.btnAct = false;
    this.actividad = new Actividades(); 
  }
  eliminarActividad(){
    this.btnAct = false;
    this.arrayActividades = this.arrayActividades.filter(x => x!= this.actividad);
    this.actividad = new Actividades();
  }


  crearEntregable(){
    this.entregable.id = this.entregable.aumentarId(this.arrayEntregables.length);
    this.arrayEntregables.push(this.entregable);
    this.entregable = new TiemposEntregables();
  }
  editarEntregable(){
    this.btnEnt = false;
    this.entregable = new TiemposEntregables(); 
  }
  eliminarEntregable(){
    this.btnEnt = false;
    this.arrayEntregables = this.arrayEntregables.filter(x => x!= this.entregable);
    this.entregable = new TiemposEntregables();
  }


}

