import { Integrantes } from './integrantes';

describe('Clase integrantes', () => {
  let inte : Integrantes;
  beforeEach(()=>{
    inte = new Integrantes();
  })
  it('Si se crea un objeto el id debe ser mayor a cero', () => {
    inte.id = inte.aumentarId(1);
    expect(inte.id > 0 ).toBeTruthy();
  });
  it('El tipo de variable del nombre debe ser string', () => {
    inte.nombre = "Prueba";
    expect(typeof inte.nombre).toEqual('string');
  });
  it('La edad del integrante debe ser un numero mayor a 18',()=>{
    inte.edad = 20;
    expect(inte.edad >= 18).toBeTruthy();
    inte.edad = 15;
    expect(inte.edad >= 18).toBeFalsy();
  });
});
