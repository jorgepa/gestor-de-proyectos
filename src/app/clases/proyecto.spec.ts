import { Proyecto } from './proyecto';

describe('Clase proyecto', () => {
  let proy : Proyecto;
  beforeEach(()=>{
    proy = new Proyecto();
  })
  it('Si se crea un objeto el id debe ser mayor a cero', () => {
    proy.id = proy.aumentarId(1);
    expect(proy.id > 0 ).toBeTruthy();
  });
  it('El tipo de variable del nombre debe ser string', () => {
    proy.nombre = "Prueba";
    expect(typeof proy.nombre).toEqual('string');
  });
});
