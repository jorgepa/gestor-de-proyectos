import { Actividades } from './actividades';
import { TestBed, async } from '@angular/core/testing';
import { Proyecto } from './proyecto';

describe('Clase actividades', () => {
  let act : Actividades;
  beforeEach(()=>{
    act = new Actividades();
  })
  it('Si se crea una actividad el id debe ser mayor a cero', () => {
    act.id = act.aumentarId(1);
    expect(act.id > 0).toBeTruthy();
  });
  it("El nombre de la variable debe ser string",()=>{
    act.nombre = "Actividad prueba"
    expect(typeof act.nombre).toEqual('string');
  })

});


