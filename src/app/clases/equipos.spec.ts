import { Equipos } from './equipos';

describe('Clase equipos', () => {
  let equ : Equipos;
  beforeEach(()=>{
    equ = new Equipos();
  })
  it('Si se crea un objeto el id debe ser mayor a cero', () => {
    equ.id = equ.aumentarId(1);
    expect(equ.id > 0 ).toBeTruthy();
  });
  it('El tipo de variable del nombre debe ser string', () => {
    equ.nombre = "Prueba";
    expect(typeof equ.nombre).toEqual('string');
  });
 

});
