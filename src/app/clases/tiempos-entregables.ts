import { Proyecto } from './proyecto';

export class TiemposEntregables extends Proyecto {
    id:number;
    nombre:string;
    tiempo:number;
}
