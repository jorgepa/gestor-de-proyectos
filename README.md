# Gestor De Proyectos

## Integrantes
-   Jorge Pacheco
-   Stefania Nieto
-   Jhon Cruz

## Descripción del proyecto

Para la elaboración de las pruebas de utlizo Jasmine como base para estas.

Las pruebas unitarias para el codigo se utilizaron bajo Karma.js mientras que las pruebas End to End (e2e) se realizaron utlizando protractor.

Para correr cada ciclo de pruebas en local se deben usar los siguientes comandos en la terminal:
- `ng test` para las pruebas unitarias
- `ng e2e` para las pruebas de interfaz e integración.

## Proceso de CI (Continuous Integration) y CD (Continuous Deployment)

Para realizar un ejercicio de CI/CD se utlizó GitLab. GitLab ofrece integración continua y despliegue continuo para los proyectos ofrecidos como públicos. Cada vez que se realiza un push sobre el repositorio se correra el proceso de CI para mostrar si este pasa o no la etapa de test y poder continuar con el proceso de CD.

### CI

En el proceso de CI se debe preparar el proyecto para que pueda desplegar un navegador desde los servidores de GitLab y poder correr las pruebas, para esto se crean 2 jobs uno de install y el otro de test, adémas de crear 2 nuevos scripts para correr las pruebas en CI.

#### Jobs para install y test

install:
  stage: install
  script: 
    - npm install
  artifacts:
    expire_in: 1h
    paths:
      - node_modules/
  cache:
    paths:
      - node_modules/


tests:
  stage: test
  variables:
    CHROME_BIN: google-chrome
  dependencies:
    - install
  before_script:
    - apt-get update && apt-get install -y apt-transport-https
    - wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
    - sh -c 'echo "deb https://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
    - apt-get update && apt-get install -y google-chrome-stable
  script:
    - npm run test:ci
    - npm run e2e:ci

  coverage: '/Statements.*?(\d+(?:\.\d+)?)%/'

  #### Scripts para pruebas
  - "test:ci": "ng test --no-watch --code-coverage --browsers=ChromeHeadlessCI" Este script corre las pruebas unitarias en una instancia de Chrome instalada en el servidor de GitLab sin que se despleigue la interfaz gráfica de este. 
  "e2e:ci": "ng e2e --protractor-config=e2e/protractor-ci.conf.js" Este script corre las pruebas de interfaz y de integración en el servidor de GitLab en la instancia de Chrome definida en el archivo protractor-ci.conf.js.

### CD

Para el proceso de CD se definen 2 jobs adicionales, build y deploy. Estos jobs solo correran desde la rama Master del proyecto y no desde cualquier otra rama prevista anteriormente. Estos jobs solo corrern en la rama Master y corren cuando un marge es aceptado

#### Jobs para build y deploy

build:
  stage: build
  variables:
    BUILD_CONFIG: 'production'
  dependencies:
    - install
  script:
    - npm run build:ci
  artifacts:
    expire_in: 1h
    paths:
      - dist/
  only:
    - master
    
pages:
  stage: deploy
  dependencies:
    - build
  script:
    - mkdir public
    - mv ./dist/gestor-de-proyectos/* ./public/
    - cp ./public/index.html public/404.html
  artifacts:
    paths:
      - public/
  environment:
    name: production
  only:
    - master