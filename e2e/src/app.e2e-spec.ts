import { AppPage } from './app.po';
import { browser, logging, element, by } from 'protractor';


describe('workspace-project App', () => {
  let page: AppPage;
  

  beforeEach(() => {
    page = new AppPage();
    page.navigateTo();
    
  });

  it('should display welcome message', () => {
  
    expect(page.getTitleText()).toEqual('GestorProyectos');
  });

  it('Prueba de la tarjeta proyectos', () =>{
    element(by.id('textoProyecto')).sendKeys('Prueba Proyecto');
    expect(element(by.id('textoProyecto')).getAttribute('value')).toBe('Prueba Proyecto');
    element(by.id('crearProyecto')).click();
    expect(element(by.id('textoProyecto')).getAttribute('value')).toBe('');
    element(by.id('tableroProyectos')).click();
    expect(element(by.id('editarProyecto')) && element(by.id('eliminarProyecto'))).toBeTruthy();
    element(by.id('textoProyecto')).clear();
    element(by.id('textoProyecto')).sendKeys('Prueba Proyecto editar');
    expect(element(by.id('textoProyecto')).getAttribute('value')).toBe('Prueba Proyecto editar');
    element(by.id('editarProyecto')).click();
    expect(element(by.id("cardText")).getText()).toBe('1Prueba Proyecto editar');
    element(by.id('tableroProyectos')).click();
    element(by.id('eliminarProyecto')).click();
    expect(element(by.id("cardText")).getText()).toBe('');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
